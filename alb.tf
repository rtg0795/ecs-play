
# ---------------------------------------------------------------------------------------------------------------------
# ALB
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_alb" "alb" {
  name            = "${var.aws_env}-${var.application}-alb"
  subnets         = "${aws_subnet.public.*.id}"
  # internal = true
  # subnets         = aws_subnet.private.*.id
  # security_groups = ["${aws_security_group.alb-sg.id}"]
  security_groups = [aws_security_group.alb-sg.id]

  tags = { Name = "${var.aws_env}-${var.application}-alb" }
}

# ---------------------------------------------------------------------------------------------------------------------
# ALB TARGET GROUP
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_alb_target_group" "trgp" {
  name        = "${var.stack}-tgrp"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    path = "/"
  }
  
  # makes user get the same instance for the specified duration
  stickiness {
    type            = "lb_cookie"
    cookie_duration = 300 #time in sec
    enabled         = true
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# ALB LISTENER
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_alb_listener" "alb-listener" {
  load_balancer_arn = aws_alb.alb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.trgp.id
    type             = "forward"
  }
}

output "alb_address" {
  value = aws_alb.alb.dns_name
}